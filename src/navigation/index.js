import React from 'react'
import { AsyncStorage } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

import { LoginScreen, HomeScreen } from '../screens'

// actions
import { saveToken } from '../redux/actions'

const Stack = createStackNavigator();

class AppNavigation extends React.Component {
  checkToken = async () => {
    try {
      let token = await AsyncStorage.getItem('token')
      console.warn("token: ", token === null)
      return token === null ? null : token
    } catch (error) {
      return null
    }
  }

  async componentDidMount() {
    let token = await this.checkToken()
    token !== null
    ? this.props.saveToken(token)
    : this.props.saveToken("")
  }

  render() {
    console.warn("auth props",this.props.auth.token)
    const token = this.props.auth.token
    return token === ""
    ? (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Login" options={{headerShown: false}} component={LoginScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      )
    : (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      )
  }
} 

// subscribe ke store
const mapStateToProps = (state) => {
  return {
    auth: state.authReducers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: (token) => dispatch(saveToken(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppNavigation);


import * as React from 'react';
import { View, Text, Button, AsyncStorage } from 'react-native';
import {connect} from 'react-redux';

import {
  saveToken
} from '../redux/actions';

class  HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      token: ""
    }
  }

  _retrieveData = async () => {
    try {
      let token = await AsyncStorage.getItem('token');
      this.setState({token})

    } catch (error) {
      // Error retrieving data
      return "error"
    }
  };

  componentDidMount() {
    this._retrieveData()
  }

  logout = () => {
    this.setState({token: ""})
    AsyncStorage.removeItem('token')
    this.props.saveToken("")
  }

  render() {
    let token = this.state.token
    console.log("HomeScreen -> render -> token", token)
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Text>Token: {token}</Text>
        <Button 
          title="Logout"
          onPress={() => this.logout()}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: (token) => dispatch(saveToken(token))
  }
}

export default connect(null, mapDispatchToProps) (HomeScreen)
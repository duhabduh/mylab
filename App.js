import React from 'react';
import { Provider } from 'react-redux'

// AppNavigation
import AppNavigation from './src/navigation';

//store
import store from './src/redux/store';

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
}

